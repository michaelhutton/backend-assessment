# Instructions
## Install
To install dependencies, please run:
`npm install`
## Test
To run any associated tests, please run:
`npm test`
## Run
From there, you can create the output.json file with:
`npm start`