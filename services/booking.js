const RATES = require('../utils/rates.json');

const failureState = ({ id, from, to }) => ({
  id,
  from,
  to,
  isValid: false,
  total: 0,
});

const successState = ({
  id, from, to, total,
}) => ({
  id,
  from,
  to,
  isValid: true,
  total,
});

// Totals sometimes round to > 2 decimal places.
const truncateTotal = number => Math.trunc(number * 100) / 100;

const BookingFactory = ({ moment }) => {
  const Booking = {
    calculateRate: (booking) => {
      // NOTE: This function does NOT check for validity.
      //       Use isValid for validity.

      // MomentJS sets start of week (0) to be Sunday
      const SUNDAY = 0;
      const SATURDAY = 6;

      const from = moment(booking.from);
      const to = moment(booking.to);
      if (from.day() === SUNDAY || to.day() === SUNDAY) {
        return RATES.sun;
      }
      if (from.day() === SATURDAY || to.day() === SATURDAY) {
        return RATES.sat;
      }
      if (
        from.hour() < 6
        || from.hour() > 20
        || to.hour() < 6
        || to.hour() > 20) {
        return RATES.night;
      }
      if (
        (from.hour() === 20
        && from.minute() > 0)
        || (to.hour() === 20
        && to.minute() > 0)
      ) {
        return RATES.night;
      }
      if (from.day() !== to.day()) {
        return RATES.night;
      }
      return RATES.day;
    },
    isValid: (booking) => {
      const from = moment(booking.from);
      const to = moment(booking.to);
      if (to.diff(from) < 0) {
        // Inverted to/from fields
        return false;
      }
      if (to.diff(from, 'hours') < 1) {
        // Worked < 1 hour
        return false;
      }
      if (to.diff(from, 'hours', true) > 24) {
        // Worked > 24 hours
        return false;
      }
      if ((from.minutes() % 15 !== 0) || (to.minutes() % 15 !== 0)) {
        // Not in 15 minute increments
        return false;
      }
      return true;
    },
    parse(booking) {
      const from = moment(booking.from);
      const to = moment(booking.to);
      if (!this.isValid(booking)) {
        return failureState(booking);
      }
      const hoursWorked = to.diff(from, 'hours', true);
      const rate = this.calculateRate(booking);
      const total = truncateTotal(hoursWorked * rate);
      return successState({
        id: booking.id,
        from: booking.from,
        to: booking.to,
        total,
      });
    },
  };
  return Booking;
};

module.exports = BookingFactory;
