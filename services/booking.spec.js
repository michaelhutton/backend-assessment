const moment = require('moment');
const bookingFactory = require('./booking');
const RATES = require('../utils/rates.json');

const booking = bookingFactory({ moment });

describe('Booking Service', () => {
  describe('Construction', () => {
    const input = {
      id: 1,
      from: '2017-10-23T08:00:00+11:00',
      to: '2017-10-23T11:00:00+11:00',
    };
    const expectedOutput = {
      id: 1,
      from: '2017-10-23T08:00:00+11:00',
      to: '2017-10-23T11:00:00+11:00',
      isValid: true,
      total: 114,
    };
    test('has methods', () => {
      expect(booking.parse).toBeDefined();
      expect(booking.isValid).toBeDefined();
      expect(booking.calculateRate).toBeDefined();
    });
    test('parses a booking and returns correct keys', () => {
      const output = booking.parse(input);
      expect(output.id).toBeDefined();
      expect(output.from).toBeDefined();
      expect(output.to).toBeDefined();
      expect(output.isValid).toBeDefined();
      expect(output.total).toBeDefined();
    });
    test('parses a booking and returns correct values (naive)', () => {
      const output = booking.parse(input);
      expect(output).toEqual(expectedOutput);
    });
  });
  describe('Business Rules', () => {
    test('invalid if booking time is under 1 hour', () => {
      const testInput = {
        id: 2,
        from: '2017-10-23T08:00:00+11:00',
        to: '2017-10-23T08:30:00+11:00',
      };
      const output = booking.parse(testInput);
      expect(output.isValid).toBeFalsy();
    });
    test('invalid if booking time is over 24 hours', () => {
      const testInput = {
        id: 2,
        from: '2017-10-23T08:00:00+11:00',
        to: '2017-10-24T08:30:00+11:00',
      };
      const output = booking.parse(testInput);
      expect(output.isValid).toBeFalsy();
    });
    test('invalid if booking ends before it begins', () => {
      const testInput = {
        id: 2,
        from: '2017-10-23T10:30:00+11:00',
        to: '2017-10-23T08:00:00+11:00',
      };
      const output = booking.parse(testInput);
      expect(output.isValid).toBeFalsy();
    });
    test('invalid if booking is not in 15 minute increments', () => {
      const testInput = {
        id: 2,
        from: '2017-10-23T10:30:00+11:00',
        to: '2017-10-23T12:14:00+11:00',
      };
      const output = booking.parse(testInput);
      expect(output.isValid).toBeFalsy();
    });
  });

  describe('Rates', () => {
    describe('Night Rates', () => {
      test('Fri 1800 - 2100', () => {
        const testInput = {
          id: 2,
          from: '2017-10-20T18:00:00+11:00',
          to: '2017-10-20T21:00:00+11:00',
        };
        const rate = booking.calculateRate(testInput);
        expect(rate).toEqual(RATES.night);
      });
      test('Wed 0500 - 1000', () => {
        const testInput = {
          id: 2,
          from: '2017-10-18T05:00:00+11:00',
          to: '2017-10-18T10:00:00+11:00',
        };
        const rate = booking.calculateRate(testInput);
        expect(rate).toEqual(RATES.night);
      });
    });
    test('rate set to day-rate if `to` = 20:00', () => {
      const testInput = {
        id: 2,
        from: '2017-10-23T19:00:00+11:00',
        to: '2017-10-23T20:00:00+11:00',
      };
      const rate = booking.calculateRate(testInput);

      expect(rate).toEqual(RATES.day);
    });
    test('rate set to night-rate if `to` = 20:15', () => {
      const testInput = {
        id: 2,
        from: '2017-10-23T19:00:00+11:00',
        to: '2017-10-23T20:15:00+11:00',
      };
      const rate = booking.calculateRate(testInput);
      expect(rate).toEqual(RATES.night);
    });
    test('times within 06:00-20:00 but on different days should be night-rate', () => {
      const testInput = {
        id: 4,
        from: '2017-10-18T18:00:00+11:00',
        to: '2017-10-19T06:00:00+11:00',
      };
      const rate = booking.calculateRate(testInput);
      expect(rate).toEqual(RATES.night);
    });
    describe('Saturday Rates', () => {
      test('Sat 1800 - 2200', () => {
        const testInput = {
          id: 2,
          from: '2017-10-21T18:00:00+11:00',
          to: '2017-10-21T22:00:00+11:00',
        };
        const rate = booking.calculateRate(testInput);
        expect(rate).toEqual(RATES.sat);
      });
    });
    describe('Sunday Rates', () => {
      test('Sun 0100 - 0700', () => {
        const testInput = {
          id: 2,
          from: '2017-10-22T01:00:00+11:00',
          to: '2017-10-22T07:00:00+11:00',
        };
        const rate = booking.calculateRate(testInput);
        expect(rate).toEqual(RATES.sun);
      });
    });
  });
});
