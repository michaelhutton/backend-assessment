const moment = require('moment');
const fs = require('fs');
const inputs = require('./input.json');
const bookingFactory = require('./services/booking');

const booking = bookingFactory({ moment });

const output = inputs.map(input => booking.parse(input));
console.log(output);

fs.writeFileSync('output.json', JSON.stringify(output, null, 2), 'utf8');
