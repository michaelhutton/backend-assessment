const moment = require('moment');
const inputs = require('./input.json');
const expectedOutput = require('./expectedOutput.json');

const bookingFactory = require('./services/booking');

const booking = bookingFactory({ moment });

test('matches expectedOutput.json file', () => {
  const output = inputs.map(input => booking.parse(input));
  expect(output).toEqual(expectedOutput);
});
